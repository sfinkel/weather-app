/*
 * FeaturePage Messages
 *
 * This contains all the text for the FeaturePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'boilerplate.containers.FeaturePage.header',
    defaultMessage: 'Features',
  },
  frontEndHeader: {
    id: 'boilerplate.containers.FeaturePage.frontEnd.header',
    defaultMessage: 'Front End',
  },
  frontEndMessage: {
    id: 'boilerplate.containers.FeaturePage.frontEnd.message',
    defaultMessage: `Written in React w/ Redux store.  
    Re-purposed the react-boilerplate project on github as it is great as a starting point, although a little complex at first glance.  
    Heavily modified it for this weather app - some variables still use the word "repos" which is what comes out of the box with react-boilerplate.  
    Also added react-google-maps and integrated it in to the boilerplate.  
    Would typically break it out to another component.  #TODOs.
    Switched out the maps api key for an unrestricted personal key.
    Still would like to do a lot more with the UI to make it pretty.`,
  },
  backendHeader: {
    id: 'boilerplate.containers.FeaturePage.backend.header',
    defaultMessage: 'Backend',
  },
  backendMessage: {
    id: 'boilerplate.containers.FeaturePage.backend.message',
    defaultMessage: `
      Node.JS backend which comes out of the box with react-boilerplate.
      Modified the server to accept a new route for location with a pair longitude, latitude coordinates.
      Also made configurable DARK_SKY_KEY environment variable which is added to container via Kubernetes deploy.
      Added a feature to mock data returned to avoid running out of Dark Sky queries based on an environment variable.
    `,
  },
  deployHeader: {
    id: 'boilerplate.containers.FeaturePage.deploy.header',
    defaultMessage: 'Deploy',
  },
  deployMessage: {
    id: 'boilerplate.containers.FeaturePage.deploy.message',
    defaultMessage: `
     The deploy cycle is this:  
     1. git push to gitlab 
     2. codefresh picks up the webhook from the push and builds the Dockerfile included in the repo 
     3. codefresh automatically handles the deploy to GCP Kubernetes cluster automatically.  That easy!
    `,
  },
  lastHeader: {
    id: 'boilerplate.containers.FeaturePage.last.header',
    defaultMessage: 'PS',
  },
  lastMessage: {
    id: 'boilerplate.containers.FeaturePage.last.message',
    defaultMessage: `Sorry, I wrote these descriptions really fast!`,
  },
  darkSkyHeader: {
    id: 'boilerplate.containers.FeaturePage.darkSky.header',
    defaultMessage: 'Weather API',
  },
  darkSkyMessage: {
    id: 'boilerplate.containers.FeaturePage.darkSky.message',
    defaultMessage: 'Powered by Dark Sky',
  },
});
