/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';

import H1 from 'components/H1';
import messages from './messages';
import List from './List';
import ListItem from './ListItem';
import ListItemTitle from './ListItemTitle';

export default class FeaturePage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  // Since state and props are static,
  // there's no need to re-render this component
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>Feature Page</title>
          <meta name="description" content="Feature page of React.js Boilerplate application" />
        </Helmet>
        <H1>
          <FormattedMessage {...messages.header} />
        </H1>
        <List>
          <ListItem>
            <ListItemTitle>
              <FormattedMessage {...messages.frontEndHeader} />
            </ListItemTitle>
            <p>
              <FormattedMessage {...messages.frontEndMessage} />
            </p>
          </ListItem>

          <ListItem>
            <ListItemTitle>
              <FormattedMessage {...messages.backendHeader} />
            </ListItemTitle>
            <p>
              <FormattedMessage {...messages.backendMessage} />
            </p>
          </ListItem>

          <ListItem>
            <ListItemTitle>
              <FormattedMessage {...messages.deployHeader} />
            </ListItemTitle>
            <p>
              <FormattedMessage {...messages.deployMessage} />
            </p>
          </ListItem>

          <ListItem>
            <ListItemTitle>
              <FormattedMessage {...messages.darkSkyHeader} />
            </ListItemTitle>
            <p>
              <a href="https://darksky.net/poweredby/"><FormattedMessage {...messages.darkSkyMessage} /></a>
            </p>
          </ListItem>

          <ListItem>
            <ListItemTitle>
              <FormattedMessage {...messages.lastHeader} />
            </ListItemTitle>
            <p>
              <FormattedMessage {...messages.lastMessage} />
            </p>
          </ListItem>
        </List>
      </div>
    );
  }
}
