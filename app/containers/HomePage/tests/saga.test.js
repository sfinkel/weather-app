/**
 * Tests for HomePage sagas
 */

import { put, takeLatest } from 'redux-saga/effects';

import { LOAD_REPOS } from 'containers/App/constants';
import { placesLoaded, placesLoadingError } from 'containers/App/actions';

import darkSky, { getPlaces } from '../saga';

//const username = 'mxstbr';

/* eslint-disable redux-saga/yield-effects */
describe('getPlaces Saga', () => {
  let getPlacesGenerator;

  // We have to test twice, once for a successful load and once for an unsuccessful one
  // so we do all the stuff that happens beforehand automatically in the beforeEach
  beforeEach(() => {
    getPlacesGenerator = getPlaces();

    const selectDescriptor = getPlacesGenerator.next().value;
    expect(selectDescriptor).toMatchSnapshot();

    const callDescriptor = getPlacesGenerator.next(username).value;
    expect(callDescriptor).toMatchSnapshot();
  });

  it('should dispatch the placesLoaded action if it requests the data successfully', () => {
    const response = [{
      name: 'First repo',
    }, {
      name: 'Second repo',
    }];
    const putDescriptor = getPlacesGenerator.next(response).value;
    expect(putDescriptor).toEqual(put(placesLoaded(response, username)));
  });

  it('should call the placesLoadingError action if the response errors', () => {
    const response = new Error('Some error');
    const putDescriptor = getPlacesGenerator.throw(response).value;
    expect(putDescriptor).toEqual(put(placesLoadingError(response)));
  });
});

describe('darkSkySaga Saga', () => {
  const darkSkySaga = darkSky();

  it('should start task to watch for LOAD_REPOS action', () => {
    const takeLatestDescriptor = darkSkySaga.next().value;
    expect(takeLatestDescriptor).toEqual(takeLatest(LOAD_REPOS, getPlaces));
  });
});
