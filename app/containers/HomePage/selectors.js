/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';

const selectHome = (state) => state.get('home');

const makeSelectUsername = () => createSelector(
  selectHome,
  (homeState) => homeState.get('username')
);

const makeSelectPlaces = () => createSelector(
  selectHome,
  (homeState) => {
    console.log('makeSelectPlaces');
    console.log(homeState.get('places'));
    return homeState.get('places')
  }
);

export {
  selectHome,
  makeSelectUsername,
  makeSelectPlaces,
};
