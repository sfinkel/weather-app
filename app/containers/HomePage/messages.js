/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  locationBox: {
    id: 'boilerplate.containers.HomePage.start_project.locationBox',
    defaultMessage: 'Irvine, CA',
  },
  weatherfor: {
    id: `boilerplate.containers.HomePage.start_project.weatherfor`,
    defaultMessage: `What's the weather like near you?`,
  },
  startProjectHeader: {
    id: 'boilerplate.containers.HomePage.start_project.header',
    defaultMessage: 'Start your next react project in seconds',
  },
  startProjectMessage: {
    id: 'boilerplate.containers.HomePage.start_project.message',
    defaultMessage: 'A highly scalable, offline-first foundation with the best DX and a focus on performance and best practices',
  },
  weatherHeader: {
    id: 'boilerplate.containers.HomePage.weather.header',
    defaultMessage: 'Irvine, CA',
  },
  trymeHeader: {
    id: 'boilerplate.containers.HomePage.tryme.header',
    defaultMessage: 'Try me!',
  },
  trymeMessage: {
    id: 'boilerplate.containers.HomePage.tryme.message',
    defaultMessage: 'Show Github repositories by',
  },
  trymeAtPrefix: {
    id: 'boilerplate.containers.HomePage.tryme.atPrefix',
    defaultMessage: '@',
  },
});
