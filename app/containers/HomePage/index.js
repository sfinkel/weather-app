/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { makeSelectRepos, makeSelectLoading, makeSelectError } from 'containers/App/selectors';
import H2 from 'components/H2';
import ReposList from 'components/ReposList';
import CenteredSection from './CenteredSection';
import Section from './Section';
import messages from './messages';
import { loadWeather } from '../App/actions';
import { changeLocation } from './actions';
import { makeSelectPlaces } from './selectors';
import reducer from './reducer';
import saga from './saga';
import Weather from 'components/AnimatedWeather';

const {withProps, lifecycle } = require("recompose");
const {
  withScriptjs,
} = require("react-google-maps");
const { StandaloneSearchBox } = require("react-google-maps/lib/components/places/StandaloneSearchBox");
const mapsApiKey = `AIzaSyDd_GYSZZGC_Za-QL4e34PCEHkc_OGmwlI`;

// TODO move this out to it's own component/container
const PlacesWithStandaloneSearchBox = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${mapsApiKey}&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `240px` }} />,
    placeholder: messages.locationBox.defaultMessage,
  }),
  lifecycle({
    componentWillMount() {
      const refs = {}

      this.setState({
        places: [],
        onSearchBoxMounted: ref => {
          refs.searchBox = ref;
        },
        onPlacesChanged: () => {
          console.log(refs.searchBox.getPlaces());
          const places = refs.searchBox.getPlaces();
          if(places && places.length > 0){
            this.setState({
              places,
            });
            this.props.onChangeLocation(places)

          }
        }
      })
    },
  }),
  withScriptjs
)(props =>
  <div data-standalone-searchbox="">
    <StandaloneSearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      onPlacesChanged={props.onPlacesChanged}
    >
      {/*TODO make a style out of this and import */}
      <input
        type="text"
        placeholder={props.placeholder}
        style={{
          boxSizing: `border-box`,
          border: `1px solid transparent`,
          width: `80%`,
          height: `32px`,
          padding: `0 12px`,
          borderRadius: `3px`,
          boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
          fontSize: `14px`,
          outline: `none`,
          textOverflow: `ellipses`,
        }}
      />
    </StandaloneSearchBox>
  </div>
);

export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {
    if (this.props.username && this.props.username.trim().length > 0) {
      this.props.onSubmitForm();
    }
  }

  render() {
    const { loading, error, repos } = this.props;
    const reposListProps = {
      loading,
      error,
      repos,
    };

    return (
      <article>
        <Helmet>
          <title>Home Page</title>
          <meta name="description" content="A little weather app" />
        </Helmet>
        <div>
          <CenteredSection>
            <H2>
              <FormattedMessage {...messages.weatherfor} />
            </H2>
            <PlacesWithStandaloneSearchBox onChangeLocation={this.props.onChangeLocation}/>
            <p>
            </p>
          </CenteredSection>
          <Section>
            <H2>
              {this.props.places[0] ? this.props.places[0].name : ''}
            </H2>
            <H2>
              <div>
              <span style={{fontWeight: 'normal'}}>{this.props.repos[0] ? ` ${parseInt(this.props.repos[0].currently.temperature)}°` : ' '}</span>
              {this.props.repos[0] &&
                <div style={{marginLeft: '20px', display: 'inline-block'}}>
                    <Weather icon={this.props.repos[0].currently.icon} size={30}/>
                </div>
              }
              </div>
            </H2>
            <ReposList {...reposListProps} />
          </Section>
        </div>
      </article>
    );
  }
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  repos: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
  onChangeLocation: PropTypes.func
};

export function mapDispatchToProps(dispatch) {
  return {
    onChangeLocation: (evt) => {
      console.log('onChangeLocation');
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(changeLocation(evt));
      dispatch(loadWeather())
    }
  };
}

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  places: makeSelectPlaces()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
