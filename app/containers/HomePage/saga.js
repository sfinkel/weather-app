/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import { LOAD_WEATHER  } from 'containers/App/constants';
import { weatherLoaded, weatherLoadedError } from 'containers/App/actions';

import request from 'utils/request';
import { makeSelectUsername, makeSelectPlaces } from 'containers/HomePage/selectors';

/**
 * Github repos request/response handler
 */
export function* getWeather() {
  console.log('*getPlaces()')
  // Select username from store
  //const username = yield select(makeSelectUsername());
  const weather = yield select(makeSelectPlaces());
  const {geometry: { location }} = weather[0];
  //console.log(JSON.stringify(weather));
  console.log(location.lng());
  //const requestURL = `https://api.github.com/users/${weather}/repos?type=all&sort=updated`;
  const port = window.location.port ? `:${window.location.port}` : '';
  const hostname = window.location.hostname ? window.location.hostname : '';
  const requestURL = `http://${hostname}${port}/location/coordinates/lat/${location.lat()}/lon/${location.lng()}`;
  console.log(requestURL);
  try {
    // Call our request helper (see 'utils/request')
    const weather = yield call(request, requestURL);
    console.log(weather);
    yield put(weatherLoaded(weather));
  } catch (err) {
    yield put(weatherLoadedError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* darkSky() {
  // Watches for LOAD_WEATHER actions and calls getPlaces() when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_WEATHER, getWeather);
}
