import { css } from 'styled-components';

const lowTempSpan = css`
  text-align: center; 
  display: inline-flex;
  color: #2476d0;
  font-weight: bold;
  padding: 5px;
`;

const highTempSpan = css`
  text-align: center; 
  display: inline-flex;
  color: #d04623;
  font-weight: bold;
  padding: 5px;
`;

export {lowTempSpan, highTempSpan}
