/**
 *
 * WeatherDayIconTemps.js
 *
 * Renders an image, enforcing the usage of the alt="" tag
 */

import React from 'react';
import PropTypes from 'prop-types';
import {HighTempSpan, LowTempSpan} from './StyledSpan'
function WeatherDayIconTemps(props) {
  return (
    <div style={{width: '100%'}}>
      <LowTempSpan>{parseInt(props.temperatureMin)}°</LowTempSpan>
      <HighTempSpan>{parseInt(props.temperatureMax)}°</HighTempSpan>
    </div>
  );
}

// We require the use of src and alt, only enforced by react in dev mode
WeatherDayIconTemps.propTypes = {
  // src: PropTypes.oneOfType([
  //   PropTypes.string,
  //   PropTypes.object,
  // ]).isRequired,
  temperatureMin: PropTypes.number.isRequired,
  temperatureMax: PropTypes.number.isRequired,
  // className: PropTypes.string,
};

export default WeatherDayIconTemps;
