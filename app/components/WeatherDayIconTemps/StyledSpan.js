import styled from 'styled-components';

import {lowTempSpan, highTempSpan} from './spanStyles';

const LowTempSpan = styled.span`${lowTempSpan}`;
const HighTempSpan = styled.span`${highTempSpan}`;

export {LowTempSpan, HighTempSpan};
