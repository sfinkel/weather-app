/**
 *
 * WeatherDayIcon.js
 *
 * Renders an image, enforcing the usage of the alt="" tag
 */

import React from 'react';
import PropTypes from 'prop-types';
import DayOfTheWeek from 'components/DayOfTheWeek'
import WeatherDayIconTemps from "components/WeatherDayIconTemps";
import Weather from 'components/AnimatedWeather'

function WeatherDayIcon(props) {
  return (
    <div style={{ margin: '0 auto', textAlign: 'center' }}>
    <DayOfTheWeek time={props.item.time}/>
      <div style={{padding: '4px'}}>
      <Weather icon={props.item.icon}/>
      </div>
    <WeatherDayIconTemps temperatureMin={props.item.temperatureMin} temperatureMax={props.item.temperatureMax}/>
    </div>
  );
}

// We require the use of src and alt, only enforced by react in dev mode
WeatherDayIcon.propTypes = {
  // src: PropTypes.oneOfType([
  //   PropTypes.string,
  //   PropTypes.object,
  // ]).isRequired,
  // item: PropTypes.number.isRequired,
  // className: PropTypes.string,
};

export default WeatherDayIcon;
