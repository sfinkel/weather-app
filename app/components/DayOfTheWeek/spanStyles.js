import { css } from 'styled-components';

const spanStyles = css`
  margin: 0 auto;
  font-weight: bold;
`;

export default spanStyles;
