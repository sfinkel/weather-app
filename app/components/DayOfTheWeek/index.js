/**
 *
 * DayOfTheWeek.js
 *
 * Renders day of the week based on date
 */

import React from 'react';
import PropTypes from 'prop-types';
import StyledSpan from './StyledSpan';
function DayOfTheWeek(props) {
  const {time, full} = props;
  const mapToDay = [
    {full: 'Sunday', short: 'Sun'},
    {full: 'Monday', short: 'Mon'},
    {full: 'Tuesday', short: 'Tue'},
    {full: 'Wednesday', short: 'Wed'},
    {full: 'Thursday', short: 'Thu'},
    {full: 'Friday', short: 'Fri'},
    {full: 'Saturday', short: 'Sat'},
    {full: 'Sunday', short: 'Sun'}
  ];

  const day = full ? mapToDay[new Date(time*1000).getDay()].full : mapToDay[new Date(time*1000).getDay()].short;
  return (
    <div style={{width: '100%'}}>
    <StyledSpan>{day}</StyledSpan>
    </div>
);
}

// We require the use of src and alt, only enforced by react in dev mode
DayOfTheWeek.propTypes = {
  // src: PropTypes.oneOfType([
  //   PropTypes.string,
  //   PropTypes.object,
  // ]).isRequired,
  time: PropTypes.number.isRequired,
  // className: PropTypes.string,
};

export default DayOfTheWeek;
