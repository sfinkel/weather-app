import styled from 'styled-components';

import spanStyles from './spanStyles';

const StyledSpan = styled.span`${spanStyles}`;

export default StyledSpan;
