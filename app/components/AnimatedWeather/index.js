import React from 'react';
import ReactAnimatedWeather from 'react-animated-weather';
const defaults = {
  icon: 'CLEAR_DAY',
  color: 'black',
  size: 40,
  animate: true
};

function Weather({icon, size}) {
  return(
    <ReactAnimatedWeather
      icon={icon.replace(/-/g, "_").toUpperCase()}
      color={defaults.color}
      size={size || defaults.size}
      animate={defaults.animate}
    />
  )
}

export default Weather;
