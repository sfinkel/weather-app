'use strict';
const _ = require('lodash');

module.exports = function({app, services}){
  if (!_.isObject(app)) {
    throw new Error('missing app parameter');
  }
  const location = require('./location')({app, services});
  app.use('/location', location);

};
