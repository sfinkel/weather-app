'use strict';
const express = require('express')
const router = express.Router();
const fs = require('fs');
module.exports = function({app, services}) {
  const {darkSky} = services;

// router.use(function timeLog (req, res, next) {
//   console.log('Time: ', Date.now());
//   next()
// });
// define the home page route
  router.get('/', function (req, res) {
    res.send('location home page');
  });

  router.get('/coordinates/lat/:lat/lon/:lon', function (req, res) {
    const {lat, lon} = req.params;
    if(process.env['SAMPLE_GET_BY_COORDINATES'] === 'ON') {
      console.log('RETURNING FAKE DATA');
      return fs.createReadStream(__dirname + '/sample_getByCoordinates.json').pipe(res);
    }
    return req.pipe(darkSky.forecast.getByCoordinates({lat, lon})).pipe(res);
    //res.send({lat, lon});
  });

// define the about route
  router.get('/about', function (req, res) {
    res.send('About');
  });

  return router;

}
