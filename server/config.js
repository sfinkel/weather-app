require('dotenv').config();
module.exports = function(){
  return {
    darkSky: {
      key: process.env['DARK_SKY_KEY']
    }
  }
};
