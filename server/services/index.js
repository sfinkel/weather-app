module.exports = function({config}){
  return {
    darkSky: require('./darksky')({config})
  }
}
