const request = require('request');

module.exports = function({config}){
  const {key} = config.darkSky;
  if(!key) {
    console.error('This request will fail, needs darkSky api key');
  }
  function getByCoordinates({lat = null, lon = null}) {
    return request.get({
      url: `https://api.darksky.net/forecast/${key}/${lat},${lon}`,
      json: true
    })
  }

  return {
    getByCoordinates
  }
};

// TODO remove callback
// will use pipe for now
//   ,function (error, response, body) {
//     if (error) {
//       return callback(new Error(error));
//     }
//     if(response.statusCode !== 200) {
//       return callback(new Error(body));
//     }
//     return callback(null, body);
//   })
