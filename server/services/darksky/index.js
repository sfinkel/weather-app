module.exports = function({config}){
  return {
    forecast: require('./forecast')({config})
  }
}
